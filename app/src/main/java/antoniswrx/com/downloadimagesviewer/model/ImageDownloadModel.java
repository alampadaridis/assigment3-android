package antoniswrx.com.downloadimagesviewer.model;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import antoniswrx.com.downloadimagesviewer.interfaces.ProvidedModelOps;
import antoniswrx.com.downloadimagesviewer.interfaces.RequiredPresenterOps;
import vandy.mooc.common.Utils;

/**
 * Created by antoniosl on 28/10/2015.
 */
public class ImageDownloadModel implements ProvidedModelOps {

    private final static String TAG = ImageDownloadModel.class.getSimpleName();

    private WeakReference<RequiredPresenterOps> mPresenter;

    private Uri mDirectoryPathName;



    @Override
    public void onCreate(RequiredPresenterOps presenter) {
        mPresenter = new WeakReference<>(presenter);
        final String timeStamp = new SimpleDateFormat("yyyyMMdd'_'HHmm").format(new Date());
        mDirectoryPathName = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/" + timeStamp + "/");
    }

    @Override
    public void onDestroy(boolean isChangingConfigurations) {

    }

    @Override
    public Uri downloadImage(Uri url) {
        if (!Utils.isExternalStorageWritable()) {
            Log.i(TAG, "External storage is not writable");
            return null;
        }
        try {
            return Utils.createDirectoryAndSaveFile(new URL(url.toString()), Uri.parse(url.getLastPathSegment()), mDirectoryPathName);
        }
        catch (MalformedURLException e) {
            Log.e(TAG, e.toString());
            return null;
        }

    }

    @Override
    public Uri getImagesRoot() {
        return mDirectoryPathName;
    }

    @Override
    public void deleteAll() {
        Log.d(TAG, "Delete all");
        File imageDirectory = new File(getImagesRoot().toString());
        File[] images = imageDirectory.listFiles();

        if (images == null) {
            return;
        }

        for (final File image : images) {
            final Uri file = Uri.parse(image.toString());
            Log.d(TAG, "deleting file " + file.toString());
            image.delete();
        }
        imageDirectory.delete();

    }

    @Override
    public int getImageCount() {
        File file = new File(mDirectoryPathName.toString());
        if (file.isDirectory()
                && file.listFiles() != null) {
            return file.listFiles().length;
        }
        return 0;
    }
}
