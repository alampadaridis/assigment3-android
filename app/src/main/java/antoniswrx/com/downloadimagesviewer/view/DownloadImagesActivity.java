package antoniswrx.com.downloadimagesviewer.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import antoniswrx.com.downloadimagesviewer.R;
import antoniswrx.com.downloadimagesviewer.interfaces.ProvidedPresenterOps;
import antoniswrx.com.downloadimagesviewer.interfaces.RequiredViewOps;
import antoniswrx.com.downloadimagesviewer.presenter.ImagePresenter;
import vandy.mooc.common.GenericActivity;
import vandy.mooc.view.DisplayImagesActivity;

/**
 * Created by antoniosl on 27/10/2015.
 */
public class DownloadImagesActivity
        extends GenericActivity<RequiredViewOps, ProvidedPresenterOps, ImagePresenter>
        implements RequiredViewOps {

    private EditText mUrlText;

    private LinearLayout mUrlLayout;

    private ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_images_activity);
        initFields();
        super.onCreate(ImagePresenter.class, this);
    }

    @Override
    protected void onDestroy() {
        getPresenter().onDestroy(isChangingConfigurations());
        super.onDestroy();
    }


    private void initFields() {
        mUrlText = (EditText) findViewById(R.id.urlTxt);
        mUrlLayout = (LinearLayout) findViewById(R.id.urlLayout);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_loading);
    }

    public void addUrl(View view) {
        final String url = mUrlText.getText().toString();
        if (URLUtil.isValidUrl(url)) {
            getPresenter().addUrl(Uri.parse(url));
            displayUrls();
        }
        else {
            Toast.makeText(this,
                    "Invalid URL",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayUrls() {
        mUrlLayout.removeAllViews();
        for (Uri url : getPresenter().getUrls()) {
            TextView textView = new TextView(this);
            textView.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setText(url.toString());
            mUrlLayout.addView(textView);
        }
        mUrlText.setText("");
    }

    public void startDownload(View view) {
        if (!getPresenter().getUrls().isEmpty()) {
            getPresenter().startDownload();
            Toast.makeText(this,
                    "Download started",
                    Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this,
                    "No images to download",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void dismissProgressBar() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void displayImages(Uri imagesRoot) {
        final Intent intent = DisplayImagesActivity.makeIntent(imagesRoot);
        Log.d(TAG, "Starting image view activity");
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Launch Activity to display the results.
            startActivity(intent);
        }
    }

    @Override
    public void reportFailedDownload(Uri url) {
        Toast.makeText(this,
                "Failed to download image " + url,
                Toast.LENGTH_SHORT).show();

    }

    public void deleteDownloadedImages(View view) {
        Toast.makeText(this,
                "Deleting files",
                Toast.LENGTH_SHORT).show();
        getPresenter().deleteImages();
    }
}
