package antoniswrx.com.downloadimagesviewer.tasks.downloader;

import android.net.Uri;

import antoniswrx.com.downloadimagesviewer.presenter.ImagePresenter;

/**
 * Created by antonis on 04/11/15.
 */
public class DownloaderWorkOrder {

    private final ImagePresenter mPresenter;

    private final Uri mUrl;


    public DownloaderWorkOrder(ImagePresenter mPresenter, Uri mUrl) {
        this.mPresenter = mPresenter;
        this.mUrl = mUrl;
    }

    public ImagePresenter getmPresenter() {
        return mPresenter;
    }

    public Uri getmUrl() {
        return mUrl;
    }
}
