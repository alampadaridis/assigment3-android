package antoniswrx.com.downloadimagesviewer.tasks.downloader;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.util.concurrent.Executor;


import antoniswrx.com.downloadimagesviewer.presenter.ImagePresenter;
import antoniswrx.com.downloadimagesviewer.tasks.filter.FilterTask;
import antoniswrx.com.downloadimagesviewer.tasks.filter.FilterWorkOrder;
import antoniswrx.com.downloadimagesviewer.tasks.filter.FilterWorkOrderResult;
import antoniswrx.com.downloadimagesviewer.tasks.filter.GrayscaleFilter;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericAsyncTaskOps;

/**
 * Created by antonis on 04/11/15.
 */
public class Downloader implements GenericAsyncTaskOps<DownloaderWorkOrder, Void, DownloaderWorkResult> {

    private final static String TAG = Downloader.class.getSimpleName();

    @Override
    public DownloaderWorkResult doInBackground(DownloaderWorkOrder... params) {
        final DownloaderWorkOrder workOrder = params[0];
        final ImagePresenter presenter = workOrder.getmPresenter();
        final Uri local = presenter.getModel().downloadImage(workOrder.getmUrl());
//        try {
//            //simulate long download
//            Thread.sleep(10000);
//        }
//        catch (InterruptedException e) {
//        }
        return new DownloaderWorkResult(workOrder.getmUrl(), local, presenter);
    }

    @Override
    public void onPostExecute(DownloaderWorkResult downloaderWorkResult) {
        if (downloaderWorkResult.getmLocal() != null) {
            final Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
            FilterTask filter = new FilterTask();
            GenericAsyncTask<FilterWorkOrder, Void, FilterWorkOrderResult, FilterTask> task = new GenericAsyncTask<>(filter);
            task.executeOnExecutor(executor, new FilterWorkOrder(new GrayscaleFilter(), downloaderWorkResult.getmUrl(), downloaderWorkResult.getmPresenter(), downloaderWorkResult.getmLocal()));
        }
        else {
            Log.i(TAG, "Failed to retrieve image at URL " + downloaderWorkResult.getmUrl());
            downloaderWorkResult.getmPresenter().onImageProcessed(downloaderWorkResult.getmUrl(), null);
        }
    }
}
