package antoniswrx.com.downloadimagesviewer.tasks.downloader;

import android.net.Uri;

import java.net.URL;

import antoniswrx.com.downloadimagesviewer.presenter.ImagePresenter;

/**
 * Created by antonis on 04/11/15.
 */
public class DownloaderWorkResult {

    private final Uri mUrl;

    private final Uri mLocal;

    private final ImagePresenter mPresenter;

    public DownloaderWorkResult(Uri mUrl, Uri mLocal, ImagePresenter mPresenter) {
        this.mUrl = mUrl;
        this.mLocal = mLocal;
        this.mPresenter = mPresenter;

    }

    public Uri getmUrl() {
        return mUrl;
    }

    public Uri getmLocal() {
        return mLocal;
    }

    public ImagePresenter getmPresenter() {
        return mPresenter;
    }
}
