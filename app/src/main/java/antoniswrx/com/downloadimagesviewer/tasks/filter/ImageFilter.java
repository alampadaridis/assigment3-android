package antoniswrx.com.downloadimagesviewer.tasks.filter;

import android.content.Context;
import android.net.Uri;

/**
 * Created by antoniosl on 06/11/2015.
 */
public interface ImageFilter {

    Uri apply (Context context, Uri pathToImageFile, Uri directoryPathname);
}
