package antoniswrx.com.downloadimagesviewer.tasks.filter;

import android.net.Uri;
import android.util.Log;

import java.io.File;

import vandy.mooc.common.GenericAsyncTaskOps;

/**
 * Created by antoniosl on 06/11/2015.
 */
public class FilterTask implements GenericAsyncTaskOps<FilterWorkOrder, Void, FilterWorkOrderResult> {

    private final static String TAG = FilterTask.class.getSimpleName();

    @Override
    public FilterWorkOrderResult doInBackground(FilterWorkOrder... params) {
        final FilterWorkOrder wo = params[0];
        ImageFilter filter = wo.getFilter();
        Uri filtered = filter.apply(wo.getmPresenter().getApplicationContext(), wo.getmLocal(), wo.getmPresenter().getModel().getImagesRoot());
        Log.d(TAG, "Filter applied, deleting original image");
        deleteImage(wo.getmLocal());
        FilterWorkOrderResult result = new FilterWorkOrderResult(wo.getmUrl(), wo.getmPresenter(), filtered);
        return result;
    }

    private void deleteImage(Uri uri) {
        File file = new File(uri.toString());
        file.delete();
    }

    @Override
    public void onPostExecute(FilterWorkOrderResult filterWorkOrderResult) {
        filterWorkOrderResult.getmPresenter().onImageProcessed(filterWorkOrderResult.getmUrl(), filterWorkOrderResult.getmFiltered());
    }
}
