package antoniswrx.com.downloadimagesviewer.tasks.filter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;

import java.io.File;
import java.io.FileOutputStream;

import vandy.mooc.common.BitmapUtils;
import vandy.mooc.common.Utils;

/**
 * Created by antoniosl on 06/11/2015.
 */
public class GrayscaleFilter implements ImageFilter {

    /**
     * Apply a grayscale filter to the @a pathToImageFile and return a
     * Uri to the filtered image.
     */
    @Override
    public Uri apply(Context context,
                                      Uri pathToImageFile,
                                      Uri directoryPathname) {
        Bitmap originalImage =
                BitmapUtils.decodeImageFromPath(context,
                        pathToImageFile);

        // Bail out if something is wrong with the image.
        if (originalImage == null)
            return null;

        Bitmap grayScaleImage =
                originalImage.copy(originalImage.getConfig(),
                        true);

        boolean hasTransparent = grayScaleImage.hasAlpha();
        int width = grayScaleImage.getWidth();
        int height = grayScaleImage.getHeight();

        // A common pixel-by-pixel grayscale conversion algorithm
        // using values obtained from en.wikipedia.org/wiki/Grayscale.
        for (int i = 0; i < height; ++i) {
            // Break out if we've been interrupted.
            if (Thread.interrupted())
                return null;

            for (int j = 0; j < width; ++j) {
                // Check if the pixel is transparent in the original
                // by checking if the alpha is 0.
                if (hasTransparent
                        && ((grayScaleImage.getPixel(j, i)
                        & 0xff000000) >> 24) == 0)
                    continue;

                // Convert the pixel to grayscale.
                int pixel = grayScaleImage.getPixel(j, i);
                int grayScale =
                        (int) (Color.red(pixel) * .299
                                + Color.green(pixel) * .587
                                + Color.blue(pixel) * .114);
                grayScaleImage.setPixel(j, i,
                        Color.rgb(grayScale,
                                grayScale,
                                grayScale));
            }
        }

        // Create a filePath to a temporary file.
        File filePath =
                new File(Utils.openDirectory(directoryPathname),
                        Utils.getUniqueFilename
                                (Uri.parse(pathToImageFile.getLastPathSegment())));

        try (FileOutputStream fileOutputStream =
                     new FileOutputStream(filePath)) {
            grayScaleImage.compress(Bitmap.CompressFormat.JPEG,
                    100,
                    fileOutputStream);

            // Create a URI from the file.
            return Uri.fromFile(filePath);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
