package antoniswrx.com.downloadimagesviewer.tasks.filter;

import android.net.Uri;

import antoniswrx.com.downloadimagesviewer.presenter.ImagePresenter;

/**
 * Created by antoniosl on 06/11/2015.
 */
public class FilterWorkOrder {

    private final ImageFilter filter;
    private final Uri mUrl;
    private final ImagePresenter mPresenter;
    private final Uri mLocal;


    public FilterWorkOrder(ImageFilter filter, Uri mUrl, ImagePresenter mPresenter, Uri mLocal) {
        this.filter = filter;
        this.mUrl = mUrl;
        this.mPresenter = mPresenter;
        this.mLocal = mLocal;
    }

    public ImageFilter getFilter() {
        return filter;
    }

    public Uri getmUrl() {
        return mUrl;
    }

    public ImagePresenter getmPresenter() {
        return mPresenter;
    }

    public Uri getmLocal() {
        return mLocal;
    }
}
