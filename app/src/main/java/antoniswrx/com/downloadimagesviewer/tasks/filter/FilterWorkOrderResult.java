package antoniswrx.com.downloadimagesviewer.tasks.filter;

import android.net.Uri;

import antoniswrx.com.downloadimagesviewer.presenter.ImagePresenter;

/**
 * Created by antoniosl on 06/11/2015.
 */
public class FilterWorkOrderResult {

    private final Uri mUrl;
    private final ImagePresenter mPresenter;
    private final Uri mFiltered;

    public FilterWorkOrderResult(Uri mUrl, ImagePresenter mPresenter, Uri mFiltered) {
        this.mUrl = mUrl;
        this.mPresenter = mPresenter;
        this.mFiltered = mFiltered;
    }

    public Uri getmUrl() {
        return mUrl;
    }

    public ImagePresenter getmPresenter() {
        return mPresenter;
    }

    public Uri getmFiltered() {
        return mFiltered;
    }
}
