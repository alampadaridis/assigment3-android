package antoniswrx.com.downloadimagesviewer.presenter;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

import antoniswrx.com.downloadimagesviewer.interfaces.ProvidedModelOps;
import antoniswrx.com.downloadimagesviewer.interfaces.ProvidedPresenterOps;
import antoniswrx.com.downloadimagesviewer.interfaces.RequiredPresenterOps;
import antoniswrx.com.downloadimagesviewer.interfaces.RequiredViewOps;
import antoniswrx.com.downloadimagesviewer.model.ImageDownloadModel;
import antoniswrx.com.downloadimagesviewer.tasks.downloader.Downloader;
import antoniswrx.com.downloadimagesviewer.tasks.downloader.DownloaderWorkOrder;
import antoniswrx.com.downloadimagesviewer.tasks.downloader.DownloaderWorkResult;
import vandy.mooc.common.GenericAsyncTask;
import vandy.mooc.common.GenericPresenter;

/**
 * Created by antoniosl on 28/10/2015.
 */
public class ImagePresenter extends GenericPresenter<RequiredPresenterOps, ProvidedModelOps, ImageDownloadModel>
                implements ProvidedPresenterOps, RequiredPresenterOps {

    private WeakReference<RequiredViewOps> mView;

    private List<Uri> mUrlList;

    private int mDownloadedImages;

    boolean mDownloadInProgress;


    @Override
    public Context getActivityContext() {
        return mView.get().getActivityContext();
    }

    @Override
    public Context getApplicationContext() {
        return mView.get().getApplicationContext();
    }

    @Override
    public void onCreate(RequiredViewOps view) {

        mView = new WeakReference<>(view);
        mUrlList = new ArrayList<>();
        super.onCreate(ImageDownloadModel.class, this);
    }

    @Override
    public void onConfigurationChange(RequiredViewOps view) {
        mView = new WeakReference<>(view);

        if (mDownloadInProgress) {
            mView.get().displayProgressBar();
        }
        mView.get().displayUrls();
    }

    @Override
    public void onDestroy(boolean isChangingConfigurations) {
        getModel().onDestroy(isChangingConfigurations);
    }

    @Override
    public List<Uri> getUrls() {
        return Collections.unmodifiableList(mUrlList);
    }

    @Override
    public void addUrl(final Uri uri) {
        mUrlList.add(uri);
    }

    @Override
    public void startDownload() {
        mDownloadInProgress = true;
        mView.get().displayProgressBar();
        final Executor executor = AsyncTask.THREAD_POOL_EXECUTOR;
        Downloader downloader = new Downloader();
        for (Uri url : mUrlList) {
            GenericAsyncTask<DownloaderWorkOrder, Void, DownloaderWorkResult, Downloader> task = new GenericAsyncTask<>(downloader);
            task.executeOnExecutor(executor, new DownloaderWorkOrder(this, url));
        }

    }

    private void reset() {
        mDownloadedImages = 0;
        mUrlList.clear();
        mView.get().displayUrls();
    }


    @Override
    public void deleteImages() {
        getModel().deleteAll();
        reset();
    }

    @Override
    public void onImageProcessed(Uri url, Uri localPath) {
        mDownloadedImages++;
        if (localPath == null) {
            mView.get().reportFailedDownload(url);
        }
        else {
            Log.d(TAG, "Received image " + localPath.toString());
        }
        showImages();
    }

    private void showImages() {
        if (allImagesDownloaded()) {
            mView.get().dismissProgressBar();
            mDownloadInProgress = false;
            reset();
            if  (getModel().getImageCount() > 0) {
                mView.get().displayImages(getModel().getImagesRoot());
            }
        }
    }

    private boolean allImagesDownloaded() {
        return mDownloadedImages == mUrlList.size();
    }
}
