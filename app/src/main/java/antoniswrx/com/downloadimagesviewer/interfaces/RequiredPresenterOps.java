package antoniswrx.com.downloadimagesviewer.interfaces;

import android.net.Uri;

import vandy.mooc.common.ContextView;

/**
 * Created by antoniosl on 28/10/2015.
 */
public interface RequiredPresenterOps extends ContextView {

    void onImageProcessed(Uri url, Uri localPath);
}
