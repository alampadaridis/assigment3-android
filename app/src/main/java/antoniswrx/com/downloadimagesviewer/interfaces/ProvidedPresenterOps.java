package antoniswrx.com.downloadimagesviewer.interfaces;

import android.net.Uri;

import java.util.List;

import vandy.mooc.common.PresenterOps;

/**
 * Created by antoniosl on 28/10/2015.
 */
public interface ProvidedPresenterOps extends PresenterOps<RequiredViewOps> {

    List<Uri> getUrls();

    void addUrl(final Uri uri);

    void startDownload();

    void deleteImages();

}
