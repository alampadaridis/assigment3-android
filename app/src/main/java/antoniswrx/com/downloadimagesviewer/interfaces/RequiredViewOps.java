package antoniswrx.com.downloadimagesviewer.interfaces;

import android.net.Uri;

import vandy.mooc.common.ContextView;

/**
 * Created by antoniosl on 28/10/2015.
 */
public interface RequiredViewOps extends ContextView {

    void displayProgressBar();

    void dismissProgressBar();

    void displayImages(final Uri imagesRoot);

    void reportFailedDownload(final Uri url);

    void displayUrls();
}
