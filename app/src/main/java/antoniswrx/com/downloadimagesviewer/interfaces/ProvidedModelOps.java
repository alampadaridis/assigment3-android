package antoniswrx.com.downloadimagesviewer.interfaces;

import android.content.Context;
import android.net.Uri;

import vandy.mooc.common.ModelOps;

/**
 * Created by antoniosl on 28/10/2015.
 */
public interface ProvidedModelOps extends ModelOps<RequiredPresenterOps> {

    /**
     * Download the image located at the provided Internet url
     * using the URL class, store it on the android file system
     * using a FileOutputStream, and return the path to the image
     * file on disk.
     *
     * @param url
     *          The URL of the image to download.
     *
     * @return
     *        Absolute path to the downloaded image file on the file
     *        system.
     */
    Uri downloadImage(Uri url);

    Uri getImagesRoot();

    void deleteAll();

    int getImageCount();
}
